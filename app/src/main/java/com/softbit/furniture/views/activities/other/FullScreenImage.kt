package com.softbit.furniture.views.activities.other

import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivityFullScreenImageBinding
import com.softbit.furniture.enums.IntentEnums
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.Handler

@Suppress("DEPRECATION")
class FullScreenImage : CustomActivity() {

    private lateinit var binding: ActivityFullScreenImageBinding

    override fun init() = Unit

    override fun setUpViewModel() = Unit

    override fun setupBinding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_full_screen_image)
        binding.handler = Handler()
        binding.imageUrl = intent.getStringExtra(IntentEnums.IMAGE.name)
        binding.lifecycleOwner = this
    }

    override fun setupObservers() = Unit

}