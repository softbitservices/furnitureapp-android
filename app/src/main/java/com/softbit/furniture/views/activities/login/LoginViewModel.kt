package com.softbit.furniture.views.activities.login

import android.app.Application
import android.content.Intent
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.databases.Session.saveUserSession
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants.INVALID_EMAIL
import com.softbit.furniture.utils.Constants.INVALID_PASSWORD
import com.softbit.furniture.utils.Constants.NO_INTERNET
import com.softbit.furniture.utils.Constants.isValidEmail
import com.softbit.furniture.utils.Constants.isValidPassword
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.activities.sign_up.SignUp
import kotlinx.coroutines.launch

class LoginViewModel(app: Application) : AndroidViewModel(app) {

    private val context by lazy { app.applicationContext }

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()

    val isLoading = MutableLiveData<Boolean>()

    val loginSuccess = MutableLiveData<Boolean>()

    init {
        email.value = "alee.ahmed730@gmail.com"
        password.value = "Aaa123"
        btnLoginClicked()
    }

    fun btnLoginClicked() {

        if (validated()) {

            try {

                viewModelScope.launch {

                    isLoading.value = true

                    val response = Repository.login(email.value.toString(), password.value.toString())

                    if (response.isSuccessful) {

                        val body = response.body()!!

                        if (body.success) {
                            context.saveUserSession(body, email.value.toString())
                            loginSuccess.postValue(true)
                        }

                        context.toast(body.message)

                    } else {
                        context.toast(response.message())
                    }

                    isLoading.value = false

                }

            } catch (e: Exception) {
                isLoading.value = false
            }

        }
    }

    fun btnRegisterClicked(view: View) {
        Intent(context, SignUp::class.java).apply { view.context.startActivity(this) }
    }

    private fun validated(): Boolean {

        val userEmail = email.value
        val userPassword = password.value

        if (userEmail.isNullOrEmpty() || !isValidEmail(userEmail)) {
            context.toast(INVALID_EMAIL)
            emailError.value = "e.g. ali@gmail.com"
            return false
        }

        if (userPassword.isNullOrEmpty() || !isValidPassword(userPassword)) {
            context.toast(INVALID_PASSWORD)
            passwordError.value = "e.g. Ali@123"
            return false
        }

        if (!context.isInternetConnected) {
            context.toast(NO_INTERNET)
            return false
        }

        return true
    }


}