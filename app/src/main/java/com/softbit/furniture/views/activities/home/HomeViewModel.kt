package com.softbit.furniture.views.activities.home

import android.app.Application
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.softbit.furniture.R
import com.softbit.furniture.views.fragments.home.HomeFragment
import com.softbit.furniture.views.fragments.profile.ProfileFragment
import com.softbit.furniture.views.fragments.search.SearchFragment
import com.softbit.furniture.views.fragments.wishlist.WishListFragment

class HomeViewModel(app: Application) : AndroidViewModel(app), BottomNavigationView.OnNavigationItemSelectedListener {

    val fragment = MutableLiveData<Fragment>()

    init {
        fragment.postValue(HomeFragment.newInstance())
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_item_home -> {
                fragment.postValue(HomeFragment.newInstance())
            }
            R.id.menu_item_search -> {
                fragment.postValue(SearchFragment.newInstance())
            }
            R.id.menu_item_wishlist -> {
                fragment.postValue(WishListFragment.newInstance())
            }
            R.id.menu_item_profile -> {
                fragment.postValue(ProfileFragment.newInstance())
            }
        }

        return true

    }


}