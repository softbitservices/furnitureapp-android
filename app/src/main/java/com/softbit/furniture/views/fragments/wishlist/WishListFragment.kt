package com.softbit.furniture.views.fragments.wishlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.FragmentWishListBinding
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.utils.CustomFragment
import com.softbit.furniture.utils.Handler
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.adapters.ProductsRecyclerAdapter

class WishListFragment : CustomFragment(), ClickListeners.ProductsItemClickListener  {

    companion object { @JvmStatic fun newInstance() = WishListFragment() }

    private lateinit var binding: FragmentWishListBinding
    private lateinit var viewModel: WishListFragmentViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(activity!!.application))
                .get(WishListFragmentViewModel::class.java)
    }

    override fun setupBinding(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wish_list, container, false)
        binding.model = viewModel
        binding.handler = Handler()
        binding.adapter = ProductsRecyclerAdapter(this, true)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun setupObservers() {
        viewModel.successfulResponse
                .observe(this, {
                    binding.adapter?.addList(it)
                })
    }

    override fun onProductItemClicked(data: ProductsData) {
        context?.toast(data.title)
    }

}