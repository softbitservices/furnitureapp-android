@file:Suppress("DEPRECATION")

package com.softbit.furniture.views.activities.sign_up

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivitySignUpBinding
import com.softbit.furniture.enums.IntentEnums
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.activities.search.SearchCity

class SignUp : CustomActivity() {

    companion object { private const val CITY_CODE = 0 }

    private lateinit var binding: ActivitySignUpBinding
    private lateinit var viewModel: SignUpViewModel

    override fun init() {

        val policy =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(getString(R.string.terms_of_policy), Html.FROM_HTML_MODE_LEGACY)
                } else {
                    Html.fromHtml(getString(R.string.terms_of_policy))
                }

        binding.cbTerms.text = policy
        binding.cbTerms.movementMethod = LinkMovementMethod.getInstance()

    }

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(SignUpViewModel::class.java)
    }

    override fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        binding.model = viewModel
        binding.lifecycleOwner = this
    }

    override fun setupObservers() {
        viewModel.cityClicked
                .observe(this, {
                    if (it != null) {
                        Intent(this, SearchCity::class.java).apply {
                            this.putExtra(IntentEnums.COUNTRY.name, it.toString())
                            startActivityForResult(this, CITY_CODE)
                        }
                    } else {
                        toast(getString(R.string.error_loading_countries))
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CITY_CODE) viewModel.city.value = data?.getStringExtra(IntentEnums.NAME.name)
        }
    }

}