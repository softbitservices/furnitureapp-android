package com.softbit.furniture.views.activities.trends

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.databases.Session.getUserId
import com.softbit.furniture.databases.Session.getUserType
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import kotlinx.coroutines.launch

class TrendsViewModel(application: Application) : AndroidViewModel(application) {

    private val context by lazy { application.applicationContext }
    val isLoadingData = MutableLiveData<Boolean>()
    val successfulResponse = MutableLiveData<ArrayList<TrendsData>>()

    init { fetchData() }

    private fun fetchData() {
        try {
            if (context.isInternetConnected) {
                viewModelScope.launch {
                    isLoadingData.value = true
                    val productsRepo = Repository.getTrends(context.getUserId(), context.getUserType())

                    if (productsRepo.isSuccessful) {
                        val trendsModel = productsRepo.body()!!
                        if (trendsModel.success) successfulResponse.postValue(trendsModel.data)
                        else context.toast(trendsModel.message)
                    } else {
                        context.toast(productsRepo.message())
                    }
                    isLoadingData.value = false
                }
            } else {
                context.toast(Constants.NO_INTERNET)
            }
        } catch (e: Exception) {
            isLoadingData.value = false
            e.message?.let { context.toast(it) }
        }
    }
}