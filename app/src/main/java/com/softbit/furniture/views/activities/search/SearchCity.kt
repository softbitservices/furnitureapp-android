package com.softbit.furniture.views.activities.search

import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivitySearchCityBinding
import com.softbit.furniture.enums.IntentEnums
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.SearchData
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.views.adapters.SearchAdapter

class SearchCity : CustomActivity(), ClickListeners.SearchItemClickListener{

    private lateinit var binding: ActivitySearchCityBinding
    private lateinit var viewModel: SearchCityViewModel

    override fun init() {
        supportActionBar?.title = getString(R.string.search)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.setCountry(intent.getStringExtra(IntentEnums.COUNTRY.name)!!)


    }

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(SearchCityViewModel::class.java)
    }

    override fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_city)
        binding.model = viewModel
        binding.adapter = SearchAdapter(this)
        binding.lifecycleOwner = this@SearchCity
    }

    override fun setupObservers() {
        viewModel.searchData.observe(this, { binding.adapter?.addList(it) })
    }

    override fun onSearchItemClicked(data: SearchData) {
        setResult(RESULT_OK, intent.apply { this.putExtra(IntentEnums.NAME.name, data.name) })
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

}