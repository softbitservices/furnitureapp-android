package com.softbit.furniture.views.activities.home

import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivityHomeBinding
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.ViewModelProviderFactory

class Home : CustomActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: HomeViewModel

    override fun init() {}

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(HomeViewModel::class.java)
    }

    override fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.model = viewModel
        binding.lifecycleOwner = this
    }

    override fun setupObservers() {
        viewModel.fragment.observe(this, {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.parent_container, it)
                    .addToBackStack(null)
                    .commit()
        })
    }
}