package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databases.Session.getUserId
import com.softbit.furniture.databases.Session.getUserType
import com.softbit.furniture.databinding.RowTrendsBinding
import com.softbit.furniture.enums.ItemType
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import kotlinx.coroutines.*

class TrendsRecyclerAdapter(private val listener: ClickListeners.TrendsItemClickListener)
    : ListAdapter<TrendsData, TrendsRecyclerAdapter.TrendsViewHolder>(Companion),
        ClickListeners.TrendsItemClickListener {

    private var productsList = ArrayList<TrendsData>()

    companion object : DiffUtil.ItemCallback<TrendsData>() {
        override fun areItemsTheSame(oldItem: TrendsData, newItem: TrendsData): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: TrendsData, newItem: TrendsData): Boolean = oldItem.t_pkid == newItem.t_pkid
    }

    inner class TrendsViewHolder(private val binding: RowTrendsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: TrendsData) {
            binding.model = data
            binding.listener = this@TrendsRecyclerAdapter
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendsViewHolder {
        return TrendsViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.row_trends,
                        parent,
                        false))
    }

    override fun onBindViewHolder(holder: TrendsViewHolder, position: Int) {
        holder.bind(productsList[holder.adapterPosition])
        holder.itemView.setOnClickListener { listener.onTrendItemClicked((productsList[holder.adapterPosition])) }
    }

    override fun getItemCount(): Int = productsList.size

    fun addList(list: ArrayList<TrendsData>) {
        this.productsList = list
        notifyDataSetChanged()
    }

    override fun onTrendItemClicked(view: View, data: TrendsData, checked: Boolean) {
        if (view.context.isInternetConnected) {
            GlobalScope.launch(Dispatchers.Main) {
                if (checked) addToFavourite(data, view)
                else removeFavourite(data, view)
            }
        } else {
            view.context.toast(Constants.NO_INTERNET)
        }
    }

    private suspend fun addToFavourite(data: TrendsData, view: View) {
        coroutineScope {
            launch {
                val repo = Repository.addFavourite(
                        view.context.getUserId(),
                        view.context.getUserType(),
                        data.t_pkid,
                        ItemType.TREND.toString())

                if (repo.isSuccessful) {
                    val model = repo.body()!!
                    if (model.success) view.context.toast(model.message)
                    else (view as CheckBox).isChecked = false
                } else {
                    (view as CheckBox).isChecked = false
                }
            }
        }
    }

    private suspend fun removeFavourite(data: TrendsData, view: View) {
        coroutineScope {
            launch {
                val repo = Repository.removeFavourite(
                        view.context.getUserId(),
                        view.context.getUserType(),
                        data.t_pkid,
                        ItemType.TREND.toString())

                if (repo.isSuccessful) {
                    val model = repo.body()!!
                    if (model.success) view.context.toast(model.message)
                    else (view as CheckBox).isChecked = true
                } else {
                    (view as CheckBox).isChecked = true
                }
            }
        }
    }

}