package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databinding.RowSearchBinding
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.SearchData

class SearchAdapter(private val listener: ClickListeners.SearchItemClickListener)
    : ListAdapter<SearchData, SearchAdapter.SearchViewHolder> (Companion) {

    private var searchList: ArrayList<SearchData> = ArrayList()

    companion object: DiffUtil.ItemCallback<SearchData>() {
        override fun areItemsTheSame(oldItem: SearchData, newItem: SearchData): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: SearchData, newItem: SearchData): Boolean = oldItem.name == newItem.name
    }

    class SearchViewHolder(private val binding: RowSearchBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: SearchData) {
            binding.data = data
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding: RowSearchBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_search, parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(searchList[position])
        holder.itemView.setOnClickListener{ listener.onSearchItemClicked(searchList[position]) }
    }

    override fun getItemCount(): Int = searchList.size

    fun addList(data: ArrayList<SearchData>) {
        searchList = data
        notifyDataSetChanged()
    }

}