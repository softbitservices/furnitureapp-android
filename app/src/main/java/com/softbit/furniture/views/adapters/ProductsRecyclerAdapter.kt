package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databases.Session.getUserId
import com.softbit.furniture.databases.Session.getUserType
import com.softbit.furniture.databinding.RowProductsBinding
import com.softbit.furniture.enums.ItemType
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import kotlinx.coroutines.*

class ProductsRecyclerAdapter(
        private val listener: ClickListeners.ProductsItemClickListener,
        private val fromWishList: Boolean = false)
    : ListAdapter<ProductsData, ProductsRecyclerAdapter.ProductsViewHolder>(Companion),
        ClickListeners.ProductsItemClickListener {

    private var productsList = ArrayList<ProductsData>()

    companion object : DiffUtil.ItemCallback<ProductsData>() {
        override fun areItemsTheSame(oldItem: ProductsData, newItem: ProductsData): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: ProductsData, newItem: ProductsData): Boolean = oldItem.p_pkid == newItem.p_pkid
    }

    inner class ProductsViewHolder(private val binding: RowProductsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ProductsData) {
            binding.model = data
            if (fromWishList) binding.cbFavourite.background = ContextCompat.getDrawable(binding.root.context, R.drawable.ic_delete)
            binding.listener = this@ProductsRecyclerAdapter
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        return ProductsViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.row_products,
                        parent,
                        false))
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        holder.bind(productsList[holder.adapterPosition])
        holder.itemView.setOnClickListener { listener.onProductItemClicked((productsList[holder.adapterPosition])) }
    }

    override fun getItemCount(): Int = productsList.size

    fun addList(list: ArrayList<ProductsData>) {
        this.productsList = list
        notifyDataSetChanged()
    }

    override fun onProductItemClicked(view: View, data: ProductsData, checked: Boolean) {
        if (view.context.isInternetConnected) {
            GlobalScope.launch(Dispatchers.Main) {
                if (checked) addToFavourite(data, view)
                else {
                    removeFavourite(data, view)
                    if (fromWishList) {
                        productsList.remove(data)
                        notifyDataSetChanged()
                    }
                }
            }
        } else {
            view.context.toast(Constants.NO_INTERNET)
        }
    }

    private suspend fun addToFavourite(data: ProductsData, view: View) {
        coroutineScope {
            launch {
                val repo = Repository.addFavourite(
                        view.context.getUserId(),
                        view.context.getUserType(),
                        data.p_pkid,
                        ItemType.PRODUCT.toString())

                if (repo.isSuccessful) {
                    val model = repo.body()!!
                    if (model.success) view.context.toast(model.message)
                    else (view as CheckBox).isChecked = false
                } else {
                    (view as CheckBox).isChecked = false
                }
            }
        }
    }

    private suspend fun removeFavourite(data: ProductsData, view: View) {
        coroutineScope {
            launch {
                val repo = Repository.removeFavourite(
                        view.context.getUserId(),
                        view.context.getUserType(),
                        data.p_pkid,
                        ItemType.PRODUCT.toString())

                if (repo.isSuccessful) {
                    val model = repo.body()!!
                    if (model.success) view.context.toast(model.message)
                    else (view as CheckBox).isChecked = true
                } else {
                    (view as CheckBox).isChecked = true
                }
            }
        }
    }

}