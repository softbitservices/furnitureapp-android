package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databinding.RowProfileBinding
import com.softbit.furniture.models.ProfileItemsModel
import com.softbit.furniture.views.adapters.ProfileItemsRecyclerAdapter.ProfileItemsViewHolder

class ProfileItemsRecyclerAdapter(private val profileItems: ArrayList<ProfileItemsModel>)
    : ListAdapter<ProfileItemsModel, ProfileItemsViewHolder>(Companion) {

    companion object: DiffUtil.ItemCallback<ProfileItemsModel>() {
        override fun areItemsTheSame(oldItem: ProfileItemsModel, newItem: ProfileItemsModel): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: ProfileItemsModel, newItem: ProfileItemsModel): Boolean = oldItem.type == newItem.type
    }

    inner class ProfileItemsViewHolder(private val binding: RowProfileBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ProfileItemsModel) {
            binding.model = data
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileItemsViewHolder {
        val binding: RowProfileBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_profile,
                parent,
                false)
        return ProfileItemsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProfileItemsViewHolder, position: Int) {
        holder.bind(profileItems[holder.adapterPosition])
    }

    override fun getItemCount(): Int = profileItems.size

}