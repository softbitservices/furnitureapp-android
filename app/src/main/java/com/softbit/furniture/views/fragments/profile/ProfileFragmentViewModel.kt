package com.softbit.furniture.views.fragments.profile

import android.app.Application
import android.content.Intent
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.softbit.furniture.databases.Session.clearUserSession
import com.softbit.furniture.databases.Session.getUserImages
import com.softbit.furniture.databases.Session.getUserSmallImages
import com.softbit.furniture.databases.Session.getUsername
import com.softbit.furniture.enums.IntentEnums
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.UserModel
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.DialogUtils.showLogoutAlert
import com.softbit.furniture.views.activities.login.Login
import com.softbit.furniture.views.activities.other.FullScreenImage
import com.softbit.furniture.views.activities.products.Products

class ProfileFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private val context by lazy { application.applicationContext }
    val user = MutableLiveData<UserModel>()

    init { fetchUserData() }

    private fun fetchUserData() {
        user.value = UserModel(
                context.getUsername(),
                context.getUserImages(),
                context.getUserSmallImages()
        )
    }

    fun onLogoutClicked(view: View) {
        view.context.showLogoutAlert(object : ClickListeners.DialogButtonClickListener {
            override fun onButtonClicked(performAction: Boolean) {
                if (performAction) {
                    context.clearUserSession()
                    Intent(view.context, Login::class.java).apply {
                        view.context.startActivity(this)
                        (view.context as CustomActivity).finishAffinity()
                    }
                }
            }
        })
    }

    fun onEditProfileClicked(view: View) {
        Intent(view.context, Products::class.java).apply {
            view.context.startActivity(this)
        }
    }

    fun onImageClicked(view: View) {
        Intent(view.context, FullScreenImage::class.java).apply {
            putExtra(IntentEnums.IMAGE.name, context.getUserImages())
            view.context.startActivity(this)
        }
    }

}