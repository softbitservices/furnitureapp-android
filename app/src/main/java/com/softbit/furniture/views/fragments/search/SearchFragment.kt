package com.softbit.furniture.views.fragments.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.FragmentHomeBinding
import com.softbit.furniture.databinding.FragmentSearchBinding
import com.softbit.furniture.utils.CustomFragment
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.views.fragments.home.HomeFragment
import com.softbit.furniture.views.fragments.home.HomeFragmentViewModel


class SearchFragment : CustomFragment() {

    companion object { @JvmStatic fun newInstance() = SearchFragment() }

    private lateinit var binding: FragmentSearchBinding
    private lateinit var viewModel: SearchFragmentViewModel

    override fun init() {

    }

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(activity!!.application))
                .get(SearchFragmentViewModel::class.java)

    }

    override fun setupBinding(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
//        binding.model = viewModel
        return binding.root
    }

    override fun setupObservers() {

    }


}