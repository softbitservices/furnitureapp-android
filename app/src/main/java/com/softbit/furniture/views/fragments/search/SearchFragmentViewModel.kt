package com.softbit.furniture.views.fragments.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class SearchFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private val context = application.applicationContext

}