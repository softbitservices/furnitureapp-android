package com.softbit.furniture.views.activities.products

import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivityProductsBinding
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.Handler
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.adapters.ProductsRecyclerAdapter

class Products : CustomActivity(), ClickListeners.ProductsItemClickListener {

    private lateinit var binding: ActivityProductsBinding
    private lateinit var viewModel: ProductsViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(ProductsViewModel::class.java)
    }

    override fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_products)
        binding.model = viewModel
        binding.handler = Handler()
        binding.adapter = ProductsRecyclerAdapter(this)
        binding.lifecycleOwner = this
    }

    override fun setupObservers() {
        viewModel.successfulResponse.observe(this, {
            binding.adapter?.addList(it)
        })
    }

    override fun onProductItemClicked(data: ProductsData) {
        toast(data.title)
    }
}