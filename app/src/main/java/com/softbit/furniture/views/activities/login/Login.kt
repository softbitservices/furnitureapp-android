package com.softbit.furniture.views.activities.login

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivityLoginBinding
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.views.activities.home.Home

class Login : CustomActivity() {

    private lateinit var viewModel: LoginViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(LoginViewModel::class.java)
    }

    override fun setupBinding() {
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.model = viewModel
        binding.lifecycleOwner = this
    }

    override fun setupObservers() {
        viewModel.loginSuccess.observe(this, {
            Intent(this, Home::class.java).apply {
                startActivity(this)
                finish()
            }
        })
    }

}