package com.softbit.furniture.views.activities.other

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.softbit.furniture.R
import com.softbit.furniture.databases.Session.isUserLoggedIn
import com.softbit.furniture.utils.Furniture
import com.softbit.furniture.views.activities.home.Home
import com.softbit.furniture.views.activities.login.Login

class Splash : AppCompatActivity() {

    private var shouldGoForward: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Furniture.SIGNATURE = System.currentTimeMillis()

        Handler(Looper.getMainLooper()).postDelayed(this::moveToNextActivity, 1000)

    }

    private fun moveToNextActivity() {
        if (shouldGoForward) {
            if(isUserLoggedIn()) startActivity(Intent(this, Home::class.java))
            else startActivity(Intent(this, Login::class.java))
            finish()
        } else {
            shouldGoForward = true
        }
    }

}