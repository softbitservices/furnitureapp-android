package com.softbit.furniture.views.fragments.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databases.LocalData
import com.softbit.furniture.databinding.FragmentProfileBinding
import com.softbit.furniture.utils.CustomFragment
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.views.adapters.ProfileItemsRecyclerAdapter


class ProfileFragment : CustomFragment() {

    companion object { @JvmStatic fun newInstance() = ProfileFragment() }

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileFragmentViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(activity!!.application))
                .get(ProfileFragmentViewModel::class.java)
    }

    override fun setupBinding(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        binding.model = viewModel
        binding.adapter = ProfileItemsRecyclerAdapter(LocalData.getProfileData())
        return binding.root
    }

    override fun setupObservers() {

    }


}