package com.softbit.furniture.views.activities.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.models.SearchData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class SearchCityViewModel(application: Application) : AndroidViewModel(application) {

    private val context by lazy { application.applicationContext }

    private var country: String = ""

    val isLoading = MutableLiveData<Boolean>()
    val search = MutableLiveData<String>()
    val searchData = MutableLiveData<ArrayList<SearchData>>()

    private var timer: Timer? = null

    fun onTextChanged(s: CharSequence) {
        timer?.cancel()
        if (s.isNotEmpty()) {
            timer = Timer()
            timer?.schedule(timerTask { if (validated()) getCities() }, 500)
        } else {
            searchData.postValue(ArrayList())
        }
    }

    private fun getCities() {
        viewModelScope.launch {
            isLoading.value = true
            val response = Repository.getCities(country, search.value.toString())

            if (response.isSuccessful) {

                val model = response.body()!!

                if (model.success) searchData.postValue(model.data)
                else context.toast(model.message)

            } else {
                context.toast(response.message())
            }

            isLoading.value = false
        }
    }

    private fun validated(): Boolean {
        if (!context.isInternetConnected) {
            context.toast(Constants.NO_INTERNET)
            return false
        }
        return true
    }

    fun btnCancelClicked() {

    }

    fun setCountry(country: String) {
        this.country = country
    }

}