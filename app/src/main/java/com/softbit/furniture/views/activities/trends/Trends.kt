package com.softbit.furniture.views.activities.trends

import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.ActivityTrendsBinding
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.utils.CustomActivity
import com.softbit.furniture.utils.Handler
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.adapters.TrendsRecyclerAdapter

class Trends : CustomActivity(), ClickListeners.TrendsItemClickListener {

    private lateinit var binding: ActivityTrendsBinding
    private lateinit var viewModel: TrendsViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(application))
                .get(TrendsViewModel::class.java)
    }

    override fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trends)
        binding.model = viewModel
        binding.handler = Handler()
        binding.adapter = TrendsRecyclerAdapter(this)
        binding.lifecycleOwner = this
    }

    override fun setupObservers() {
        viewModel.successfulResponse.observe(this, {
            binding.adapter?.addList(it)
        })
    }

    override fun onTrendItemClicked(data: TrendsData) {
        toast(data.title)
    }

}