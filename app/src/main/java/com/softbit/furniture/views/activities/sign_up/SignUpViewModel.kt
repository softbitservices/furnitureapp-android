package com.softbit.furniture.views.activities.sign_up

import android.app.Application
import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.enums.SignUpType
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.*
import kotlinx.coroutines.launch


class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    companion object { private const val TAG = "SignUpViewModel" }

    private val context: Context by lazy { application.applicationContext }

    val cityClicked = MutableLiveData<String>()

    val countries = MutableLiveData<List<String>>()
    val selectedCountry = MutableLiveData<Int>()

    val name = MutableLiveData<String>()
    val city = MutableLiveData<String>()

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()

    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()

    private val isChecked = MutableLiveData<Boolean>()

    val isLoading = MutableLiveData<Boolean>()
    val isLoadingCountries = MutableLiveData<Boolean>()

    init {
        isChecked.value = false
        getCountriesFromApi()
    }

    private fun getCountriesFromApi() {
        if (context.isInternetConnected) {
            isLoadingCountries.value = true
            viewModelScope.launch {
                val response = Repository.getCountries()

                if (response.isSuccessful) {

                    val model = response.body()!!

                    Log.e(TAG, "getCountries: $model")

                    if (model.success) {
                        val countriesName = ArrayList<String>()
                        model.data.forEach { countriesName.add(it.name) }
                        countries.value = countriesName
                    }

                } else {
                    context.toast(response.message())
                }

                isLoadingCountries.value = false

            }
        }
    }

    private fun validated(): Boolean {

        val userEmail = email.value
        val userPassword = password.value

        if (name.value.isNullOrEmpty()) {
            context.toast("Please enter your name.")
            return false
        }

        if (userEmail.isNullOrEmpty() || !Constants.isValidEmail(userEmail)) {
            context.toast(Constants.INVALID_EMAIL)
            emailError.value = "e.g. ali@gmail.com"
            return false
        }


        if (countries.value.isNullOrEmpty()) {
            context.toast("Please choose country from list.")
            return false
        }


        if (city.value.isNullOrEmpty()) {
            context.toast("Please choose city.")
            return false
        }


        if (userPassword.isNullOrEmpty() || !Constants.isValidPassword(userPassword)) {
            context.toast(Constants.INVALID_PASSWORD)
            passwordError.value = "e.g. Ali@123"
            return false
        }

        if (userPassword.toString() != confirmPassword.value.toString()) {
            context.toast("Passwords not matched")
            return false
        }

        if (!isChecked.value!!) {
            context.toast("Please agree to terms and conditions to continue.")
            return false
        }

        if (!context.isInternetConnected) {
            context.toast(Constants.NO_INTERNET)
            return false
        }

        return true
    }

    fun onCheckedChanged(checked: Boolean) {
        isChecked.value = checked
    }

    fun onSelectItem() {
        city.value = ""
    }

    fun btnRegisterClicked(view: View) {
        if (validated()) {
            viewModelScope.launch {
                isLoading.value = true

                val response = Repository.registerUser(
                        name.value.toString(),
                        email.value.toString(),
                        password.value.toString(),
                        countries.value?.get(selectedCountry.value!!).toString(),
                        city.value.toString(),
                        context.getFirebaseToken(),
                        SignUpType.SERVER.toString()
                )

                if (response.isSuccessful) {

                    val model = response.body()!!

                    Log.e(TAG, "btnRegisterClicked: $model")

                    if (model.success) (view.context as CustomActivity).finish()

                    context.toast(model.message)

                } else {
                    context.toast(response.message())
                }

                isLoading.value = false

            }
        }
    }

    fun btnCityClicked() {
        cityClicked.value = countries.value?.get(selectedCountry.value!!)
    }

}