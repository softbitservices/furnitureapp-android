package com.softbit.furniture.views.fragments.home

import android.app.Application
import android.content.Intent
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.softbit.furniture.databases.Session.getUserId
import com.softbit.furniture.databases.Session.getUserType
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.network.Repository
import com.softbit.furniture.utils.Constants
import com.softbit.furniture.utils.isInternetConnected
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.activities.products.Products
import com.softbit.furniture.views.activities.trends.Trends
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class HomeFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private val context = application.applicationContext
    val isLoadingData = MutableLiveData<Boolean>()
    val successfulResponse = MutableLiveData<Pair<ArrayList<ProductsData>, ArrayList<TrendsData>>>()

    init { fetchData() }

    private fun fetchData() {
        try {

            if (context.isInternetConnected) {

                viewModelScope.launch {

                    isLoadingData.value = true

                    val productsAsync = async { Repository.getProducts(context.getUserId(), context.getUserType()) }
                    val trendsAsync = async { Repository.getTrends(context.getUserId(), context.getUserType()) }

                    val productsResponse = productsAsync.await()
                    val trendsResponse = trendsAsync.await()

                    if (productsResponse.isSuccessful) {

                        val productsModel = productsResponse.body()!!
                        val trendsModel = trendsResponse.body()!!

                        if (productsModel.success) {
                            successfulResponse.postValue(Pair(productsModel.data, trendsModel.data))
                        } else {
                            context.toast(productsModel.message)
                        }

                    } else {
                        context.toast(productsResponse.message())
                    }
                    isLoadingData.value = false
                }
            } else {
                context.toast(Constants.NO_INTERNET)
            }
        } catch (e: Exception) {
            isLoadingData.value = false
            e.message?.let { context.toast(it) }
        }
    }

    fun viewAllProductsClicked(view: View) {
        Intent(context, Products::class.java).apply { view.context.startActivity(this) }
    }

    fun viewAllTrendsClicked(view: View) {
        Intent(context, Trends::class.java).apply { view.context.startActivity(this) }
    }

}