package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databinding.RowHomeProductsBinding
import com.softbit.furniture.interfaces.ClickListeners
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.views.adapters.HomeProductsRecyclerAdapter.HomeProductsViewHolder

class HomeProductsRecyclerAdapter(private val listener: ClickListeners.ProductsItemClickListener)
    : ListAdapter<ProductsData, HomeProductsViewHolder>(Companion) {

    private var productsList = ArrayList<ProductsData>()

    companion object: DiffUtil.ItemCallback<ProductsData>() {
        override fun areItemsTheSame(oldItem: ProductsData, newItem: ProductsData): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: ProductsData, newItem: ProductsData): Boolean = oldItem.p_pkid == newItem.p_pkid
    }

    inner class HomeProductsViewHolder(private val binding: RowHomeProductsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ProductsData) {
            binding.model = data
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeProductsViewHolder {
        val binding: RowHomeProductsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_home_products,
                parent,
                false)
        return HomeProductsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeProductsViewHolder, position: Int) {
        holder.bind(productsList[holder.adapterPosition])
        holder.itemView.setOnClickListener { listener.onProductItemClicked(productsList[holder.adapterPosition]) }
    }

    override fun getItemCount(): Int = productsList.size

    fun addList(productsList: ArrayList<ProductsData>) {
        this.productsList = productsList
        notifyDataSetChanged()
    }
}