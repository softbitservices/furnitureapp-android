package com.softbit.furniture.views.fragments.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softbit.furniture.R
import com.softbit.furniture.databinding.FragmentHomeBinding
import com.softbit.furniture.interfaces.ClickListeners.ProductsItemClickListener
import com.softbit.furniture.interfaces.ClickListeners.TrendsItemClickListener
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.utils.CustomFragment
import com.softbit.furniture.utils.ViewModelProviderFactory
import com.softbit.furniture.utils.toast
import com.softbit.furniture.views.adapters.HomeProductsRecyclerAdapter
import com.softbit.furniture.views.adapters.HomeTrendsRecyclerAdapter

class HomeFragment : CustomFragment(), ProductsItemClickListener, TrendsItemClickListener {

    companion object { @JvmStatic fun newInstance() = HomeFragment() }

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeFragmentViewModel

    override fun init() = Unit

    override fun setUpViewModel() {
        viewModel = ViewModelProviders
                .of(this, ViewModelProviderFactory(activity!!.application))
                .get(HomeFragmentViewModel::class.java)
    }

    override fun setupBinding(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.model = viewModel
        binding.adapterProducts = HomeProductsRecyclerAdapter(this)
        binding.adapterTrends = HomeTrendsRecyclerAdapter(this)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun setupObservers() {
        viewModel.successfulResponse.observe(this, {
            binding.adapterProducts?.addList(it.first)
            binding.adapterTrends?.addList(it.second)
        })
    }

    override fun onProductItemClicked(data: ProductsData) {
        context?.toast(data.title)
    }

    override fun onTrendItemClicked(data: TrendsData) {
        context?.toast(data.title)
    }

}