package com.softbit.furniture.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.softbit.furniture.R
import com.softbit.furniture.databinding.RowHomeTrendsBinding
import com.softbit.furniture.interfaces.ClickListeners.TrendsItemClickListener
import com.softbit.furniture.models.TrendsData
import com.softbit.furniture.views.adapters.HomeTrendsRecyclerAdapter.TrendsViewHolder

class HomeTrendsRecyclerAdapter(private val listener: TrendsItemClickListener)
    : ListAdapter<TrendsData, TrendsViewHolder>(Companion) {

    private var trendsList = ArrayList<TrendsData>()

    companion object : DiffUtil.ItemCallback<TrendsData>() {
        override fun areItemsTheSame(oldItem: TrendsData, newItem: TrendsData): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: TrendsData, newItem: TrendsData): Boolean = oldItem.t_pkid == newItem.t_pkid
    }

    inner class TrendsViewHolder(private val binding: RowHomeTrendsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(trendsData: TrendsData) {
            binding.model = trendsData
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendsViewHolder {
        val binding: RowHomeTrendsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_home_trends,
                parent,
                false)
        return TrendsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TrendsViewHolder, position: Int) {
        holder.bind(trendsList[holder.adapterPosition])
        holder.itemView.setOnClickListener { listener.onTrendItemClicked(trendsList[holder.adapterPosition]) }
    }

    override fun getItemCount(): Int = trendsList.size

    fun addList(trendsList: ArrayList<TrendsData>) {
        this.trendsList = trendsList
        notifyDataSetChanged()
    }
}