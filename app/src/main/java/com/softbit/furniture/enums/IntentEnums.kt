package com.softbit.furniture.enums

enum class IntentEnums {
    NAME,
    COUNTRY,
    IMAGE,
}