package com.softbit.furniture.enums

enum class ProfileItems {
    CREATE_POST,
    MY_PRODUCTS,
    ACCOUNTS,
    ABOUT_US
}