package com.softbit.furniture.enums

enum class SignUpType {

    SERVER, FACEBOOK, GOOGLE;

    override fun toString(): String {
        return when (this) {
            SERVER -> "0"
            FACEBOOK -> "1"
            GOOGLE -> "2"
        }
    }}