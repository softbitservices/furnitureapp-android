package com.softbit.furniture.enums

enum class ItemType {

    TREND, PRODUCT;

    override fun toString(): String {
        return when (this) {
            TREND -> "0"
            PRODUCT -> "1"
        }
    }}