package com.softbit.furniture.enums

enum class UserType {

    USER, BUSINESS;

    override fun toString(): String {
        return when (this) {
            USER -> "0"
            BUSINESS -> "1"
        }
    }}