package com.softbit.furniture.models

data class TrendsModel(
        val case: String,
        val `data`: ArrayList<TrendsData>,
        val message: String,
        val success: Boolean
)

data class TrendsData(
    val cat_name: String,
    val cat_pkid: String,
    val date: String,
    val description: String,
    val images: String,
    val is_favourite: String,
    val small_images: String,
    val t_pkid: String,
    val title: String
)