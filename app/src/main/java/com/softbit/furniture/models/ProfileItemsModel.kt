package com.softbit.furniture.models

import android.view.View
import androidx.annotation.StringRes
import com.softbit.furniture.enums.ProfileItems
import com.softbit.furniture.enums.ProfileItems.*
import com.softbit.furniture.utils.toast

data class ProfileItemsModel (val image: Int, @StringRes val title: Int, val type: ProfileItems){
    fun onItemClicked(view: View, item: ProfileItems) {
        when (item) {
            CREATE_POST -> {
                view.context.toast("create post")
            }
            MY_PRODUCTS -> {
                view.context.toast("products")
            }
            ACCOUNTS -> {
                view.context.toast("accounts")
            }
            ABOUT_US -> {
                view.context.toast("about us")
            }
        }
    }
}