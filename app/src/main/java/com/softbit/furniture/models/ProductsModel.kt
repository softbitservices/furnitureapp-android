package com.softbit.furniture.models

data class ProductsModel(
        val case: String,
        val `data`: ArrayList<ProductsData>,
        val message: String,
        val success: Boolean
)

data class ProductsData(
    val availability: String,
    val b_pkid: String,
    val cat_name: String,
    val cat_pkid: String,
    val city: String,
    val col_code: String,
    val col_name: String,
    val col_pkid: String,
    val country: String,
    val date: String,
    val description: String,
    val dimensions: String,
    val disc_price: String,
    val rating: String,
    val images: String,
    val is_favourite: String,
    val mat_name: String,
    val mat_pkid: String,
    val name: String,
    val p_pkid: String,
    val price: String,
    val small_images: String,
    val sub_cat_name: String,
    val sub_cat_pkid: String,
    val title: String,
    val weight: String,
    val weight_unit: String
){
    fun getAvailabilityText() : String {
        return when (availability) {
            "1" -> "In Stock"
            "0" -> "Out of Stock"
            else -> "Out of Stock"
        }
    }
}