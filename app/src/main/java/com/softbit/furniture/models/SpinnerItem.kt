package com.softbit.furniture.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.softbit.furniture.BR

class SpinnerItem : BaseObservable() {

    private var selectedItemPosition = 0

    @Bindable
    fun getSelectedItemPosition() : Int {
        return selectedItemPosition
    }

    fun setSelectedItemPosition(selectedItemPosition: Int) {
        this.selectedItemPosition = selectedItemPosition
        notifyPropertyChanged(BR.selectedItemPosition)
    }

}