package com.softbit.furniture.models

data class CountriesModel(
        val case: String,
        val `data`: List<CountriesData>,
        val message: String,
        val success: Boolean
)

data class CountriesData(
//        val country_code: String,
//        val flag: String,
        val name: String
)