package com.softbit.furniture.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

data class SearchModel(
        val case: String,
        val `data`: ArrayList<SearchData>,
        val message: String,
        val success: Boolean
)

@Parcelize
data class SearchData(
    val name: String
) : Parcelable