package com.softbit.furniture.models

data class LoginModel(
    val case: String,
    val city: String,
    val country: String,
    val gender: String,
    val images: String,
    val small_images: String,
    val message: String,
    val sign_up_type: String,
    val success: Boolean,
    val user_pkid: String,
    val username: String,
    val user_type: String,
)