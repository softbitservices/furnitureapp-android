package com.softbit.furniture.models

data class UserModel (
        val username: String,
        val images: String,
        val small_images: String,
)