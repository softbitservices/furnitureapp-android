package com.softbit.furniture.network

object Params {

    const val API_CASE = "case"
    const val EMAIL = "email"
    const val PASSWORD = "password"
    const val USERNAME = "username"
    const val USER_ID = "user_pkid"
    const val BUSINESS_ID = "b_pkid"
    const val TREND_ID = "t_pkid"
    const val FAVOURITE_ID = "fav_pkid"
    const val ITEM_ID = "item_pkid"
    const val ITEM_TYPE = "item_type"
    const val CITY = "city"
    const val COUNTRY = "country"
    const val DEVICE_ID = "device_id"
    const val USER_TYPE = "user_type"
    const val SIGN_UP_TYPE = "sign_up_type"
    const val KEYWORD = "keyword"
    const val PAGE = "page"



}