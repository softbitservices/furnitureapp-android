package com.softbit.furniture.network

import com.softbit.furniture.models.*
import com.softbit.furniture.network.ApiProvider.Companion.retrofit
import retrofit2.Response

object Repository {

    suspend fun login(email: String, password: String): Response<LoginModel> {
        return retrofit.login(Cases.LOGIN, email, password)
    }

    suspend fun registerUser(name: String, email: String, password: String, country: String, city: String,
                             deviceId: String, signUpType: String): Response<BaseModel> {
        return retrofit.registerUser(Cases.REGISTER_USER, name, email, password, country, city, deviceId, signUpType)
    }

    suspend fun getCountries(): Response<CountriesModel> {
        return retrofit.getCountries(Cases.COUNTRIES)
    }

    suspend fun getCities(country: String, keyword: String): Response<SearchModel> {
        return retrofit.getCities(Cases.CITIES, country, keyword)
    }

    suspend fun getTrends(userId: String, userType: String, page: String = "0"): Response<TrendsModel> {
        return retrofit.getTrends(Cases.TRENDS, userId, userType, page)
    }

    suspend fun getProducts(userId: String, userType: String, page: String = "0"): Response<ProductsModel> {
        return retrofit.getProducts(Cases.PRODUCTS, userId, userType, page)
    }

    suspend fun getFavouritesTrends(userId: String, userType: String): Response<TrendsModel> {
        return retrofit.getFavouriteTrends(Cases.FAVOURITE_TRENDS, userId, userType)
    }

    suspend fun getFavouritesProducts(userId: String, userType: String): Response<ProductsModel> {
        return retrofit.getFavouriteProducts(Cases.FAVOURITE_PRODUCTS, userId, userType)
    }

    suspend fun addFavourite(userId: String, userType: String, itemId: String, itemType: String): Response<BaseModel> {
        return retrofit.addFavourite(Cases.ADD_FAVOURITE, userId, userType, itemId, itemType)
    }

    suspend fun removeFavourite(userId: String, userType: String, itemId: String, itemType: String): Response<BaseModel> {
        return retrofit.removeFavourite(Cases.REMOVE_FAVOURITE, userId, userType, itemId, itemType)
    }

}