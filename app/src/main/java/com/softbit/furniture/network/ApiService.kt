package com.softbit.furniture.network

import com.softbit.furniture.models.*
import com.softbit.furniture.utils.Constants.BASE_URL_PATH
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

//******************************************************************************************************************************************************
    /* Login */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun login(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.EMAIL) email: String,
            @Field(Params.PASSWORD) password: String
    ): Response<LoginModel>

//******************************************************************************************************************************************************
    /* Register User */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun registerUser(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USERNAME) name: String,
            @Field(Params.EMAIL) email: String,
            @Field(Params.PASSWORD) password: String,
            @Field(Params.COUNTRY) country: String,
            @Field(Params.CITY) city: String,
            @Field(Params.DEVICE_ID) deviceId: String,
            @Field(Params.SIGN_UP_TYPE) signUpType: String,
    ): Response<BaseModel>



//******************************************************************************************************************************************************
    /* Get All Countries */
//******************************************************************************************************************************************************

    @GET(BASE_URL_PATH)
    suspend fun getCountries(
            @Query(Params.API_CASE) xCase: String
    ): Response<CountriesModel>


//******************************************************************************************************************************************************
    /* Get Cities using Country and text */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun getCities(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.COUNTRY) country: String,
            @Field(Params.KEYWORD) keyword: String,
    ): Response<SearchModel>


//******************************************************************************************************************************************************
    /* Get Trends */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun getTrends(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
            @Field(Params.PAGE) page: String,
    ): Response<TrendsModel>


//******************************************************************************************************************************************************
    /* Get Products */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun getProducts(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
            @Field(Params.PAGE) page: String,
    ): Response<ProductsModel>


//******************************************************************************************************************************************************
    /* Get Favourite Trends */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun getFavouriteTrends(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
    ): Response<TrendsModel>


//******************************************************************************************************************************************************
    /* Get Favourite Products */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun getFavouriteProducts(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
    ): Response<ProductsModel>


//******************************************************************************************************************************************************
    /* Add Favourite */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun addFavourite(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
            @Field(Params.ITEM_ID) itemId: String,
            @Field(Params.ITEM_TYPE) itemType: String,
    ): Response<BaseModel>


//******************************************************************************************************************************************************
    /* Remove Favourite */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(BASE_URL_PATH)
    suspend fun removeFavourite(
            @Field(Params.API_CASE) xCase: String,
            @Field(Params.USER_ID) userId: String,
            @Field(Params.USER_TYPE) userType: String,
            @Field(Params.ITEM_ID) itemId: String,
            @Field(Params.ITEM_TYPE) itemType: String,
    ): Response<BaseModel>



//******************************************************************************************************************************************************
//******************************************************************************************************************************************************


}