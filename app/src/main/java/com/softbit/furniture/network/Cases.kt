package com.softbit.furniture.network

object Cases {

    const val LOGIN = "authenticate_user"
    const val REGISTER_USER = "register_user"
    const val COUNTRIES = "get_countries"
    const val CITIES = "search_cities_using_country"
    const val TRENDS = "get_trends"
    const val PRODUCTS = "get_products"
    const val FAVOURITE_PRODUCTS = "get_favourite_products"
    const val FAVOURITE_TRENDS = "get_favourite_trends"
    const val ADD_FAVOURITE = "add_favourite"
    const val REMOVE_FAVOURITE = "remove_favourite"

}