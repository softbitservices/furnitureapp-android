@file:Suppress("UNCHECKED_CAST")

package com.softbit.furniture.utils

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.softbit.furniture.views.fragments.home.HomeFragmentViewModel
import com.softbit.furniture.views.fragments.profile.ProfileFragmentViewModel
import com.softbit.furniture.views.fragments.search.SearchFragmentViewModel
import com.softbit.furniture.views.fragments.wishlist.WishListFragmentViewModel
import com.softbit.furniture.views.activities.home.HomeViewModel
import com.softbit.furniture.views.activities.login.LoginViewModel
import com.softbit.furniture.views.activities.products.ProductsViewModel
import com.softbit.furniture.views.activities.search.SearchCityViewModel
import com.softbit.furniture.views.activities.sign_up.SignUpViewModel
import com.softbit.furniture.views.activities.trends.TrendsViewModel

class ViewModelProviderFactory(private val app: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return when {

            modelClass.isAssignableFrom(LoginViewModel::class.java) -> LoginViewModel(app) as T
            modelClass.isAssignableFrom(SignUpViewModel::class.java) -> SignUpViewModel(app) as T
            modelClass.isAssignableFrom(SearchCityViewModel::class.java) -> SearchCityViewModel(app) as T
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> HomeViewModel(app) as T
            modelClass.isAssignableFrom(HomeFragmentViewModel::class.java) -> HomeFragmentViewModel(app) as T
            modelClass.isAssignableFrom(ProfileFragmentViewModel::class.java) -> ProfileFragmentViewModel(app) as T
            modelClass.isAssignableFrom(SearchFragmentViewModel::class.java) -> SearchFragmentViewModel(app) as T
            modelClass.isAssignableFrom(WishListFragmentViewModel::class.java) -> WishListFragmentViewModel(app) as T
            modelClass.isAssignableFrom(ProductsViewModel::class.java) -> ProductsViewModel(app) as T
            modelClass.isAssignableFrom(TrendsViewModel::class.java) -> TrendsViewModel(app) as T

            else -> throw UnsupportedOperationException("Please initialize ViewModel in ViewModelProviderFactory::class")

        }
    }
}