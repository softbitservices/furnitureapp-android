package com.softbit.furniture.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.softbit.furniture.R
import com.softbit.furniture.interfaces.ClickListeners

object DialogUtils {

    fun Context.showLogoutAlert(listener: ClickListeners.DialogButtonClickListener) {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(getString(R.string.are_you_sure_you_want_to_logout))
        dialog.setPositiveButton(getString(R.string.yes)) { _, _-> listener.onButtonClicked(true)}
        dialog.setNegativeButton(getString(R.string.no)) { _, _-> listener.onButtonClicked(false)}
        dialog.show()
    }

}