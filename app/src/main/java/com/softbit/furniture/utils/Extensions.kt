package com.softbit.furniture.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import com.softbit.furniture.R
import com.softbit.furniture.databases.Session


fun Context.getFirebaseToken(): String {
    return ""
}

fun Context.getUserPreferences() : SharedPreferences {
    return getSharedPreferences(Session.USER_PREFERENCES, Context.MODE_PRIVATE)
}

fun Context.getUserEditor() : SharedPreferences.Editor {
    return getUserPreferences().edit()
}

fun Context.toast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.toast(message: Int) = Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show()

val Context.isInternetConnected: Boolean get() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false

        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }

    } else {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
                .activeNetworkInfo?.isConnectedOrConnecting == true
    }
}

/*

fun <T : Application> AndroidViewModel.hasInternetConnection(): Boolean {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

        val connectivityManager = getApplication<T>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false

        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }

    } else {
        return (getApplication<T>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
                .activeNetworkInfo?.isConnectedOrConnecting == true
    }
}
*/
