package com.softbit.furniture.utils

import android.app.Activity
import android.view.View

class Handler {

    fun onBackButtonClicked(view: View) = (view.context as Activity).finish()

}