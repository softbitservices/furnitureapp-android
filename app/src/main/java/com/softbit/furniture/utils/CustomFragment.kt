package com.softbit.furniture.utils

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class CustomFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setUpViewModel()
        val binding = setupBinding(inflater, container)
        init()
        setupObservers()

        return binding
    }

    abstract fun init()
    abstract fun setUpViewModel()
    abstract fun setupBinding(inflater: LayoutInflater, container: ViewGroup?): View?
    abstract fun setupObservers()

}