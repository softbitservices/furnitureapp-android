package com.softbit.furniture.utils

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class CustomActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpViewModel()
        setupBinding()
        init()
        setupObservers()

    }

    abstract fun init()
    abstract fun setUpViewModel()
    abstract fun setupBinding()
    abstract fun setupObservers()

}