package com.softbit.furniture.utils

import java.util.regex.Pattern


object Constants {

    const val BASE_URL = "https://furniture.travelfic.com/"
    const val BASE_URL_PATH = "apis/furniture_app.php"

    const val NO_INTERNET = "No internet connection. Please connect to internet to continue."
    const val INVALID_EMAIL = "Invalid Email\nPlease follow this pattern\n(name@domain.com)"
    const val INVALID_PASSWORD = "Invalid Password\nPassword should be alphanumeric at least 6 in length, including Upper/Lowercase & number."

    fun isValidEmail(email: String): Boolean {
        val emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return Pattern.compile(emailPattern).matcher(email).matches()
    }

    fun isValidPassword(password: String): Boolean {
        val passwordPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{6,}$"
        return Pattern.compile(passwordPattern).matcher(password).matches()
    }

}