
package com.softbit.furniture.utils

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import com.softbit.furniture.R
import com.softbit.furniture.views.adapters.*
import de.hdodenhof.circleimageview.CircleImageView

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(imageView: ImageView, imageUrl: String) {
        val singleImage = imageUrl.split(";")[0]
        val baseUrl = if (singleImage.contains(Constants.BASE_URL)) "" else Constants.BASE_URL
        GlideApp.with(imageView)
                .load(baseUrl + singleImage)
                .placeholder(R.drawable.ic_place_holder_image)
                .error(R.drawable.ic_place_holder_image)
                .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(imageView: CircleImageView, imageUrl: String) {
        val singleImage = imageUrl.split(";")[0]
        val baseUrl = if (singleImage.contains(Constants.BASE_URL)) "" else Constants.BASE_URL
        GlideApp.with(imageView)
                .load(baseUrl + singleImage)
                .placeholder(R.drawable.ic_place_holder_image)
                .error(R.drawable.ic_place_holder_image)
                .into(imageView)
    }

    @JvmStatic
    @BindingAdapter(value = ["loadImage", "loader"], requireAll = true)
    fun loadImage(imageView: PhotoView, imageUrl: String, progressBar: ProgressBar) {
        val singleImage = imageUrl.split(";")[0]
        val baseUrl = if (singleImage.contains(Constants.BASE_URL)) "" else Constants.BASE_URL
        GlideApp.with(imageView)
                .load(baseUrl + singleImage)
                .error(R.drawable.ic_place_holder_image)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }
                })
                .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(imageView: ImageView, @DrawableRes imageDrawable: Int) {
        GlideApp.with(imageView)
                .load(ContextCompat.getDrawable(imageView.context, imageDrawable))
                .error(R.drawable.ic_place_holder_image)
                .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("onNavigationItemSelected")
    fun setOnNavigationItemSelected(view: BottomNavigationView, listener: BottomNavigationView.OnNavigationItemSelectedListener?) {
        view.setOnNavigationItemSelectedListener(listener)
    }

//    @JvmStatic
//    @BindingAdapter("selectedItemPosition")
//    fun setSelectedItemPosition(view: BottomNavigationView, position: Int, ) {
//        view.selectedItemId = position
//    }

    @JvmStatic
    @BindingAdapter("setError")
    fun setError(inputEditText: TextInputEditText, error: MutableLiveData<String>) {
        inputEditText.error = error.value
    }

    @JvmStatic
    @BindingAdapter("setFavourite")
    fun setFavourite(checkBox: CheckBox, value: String) {
        checkBox.isChecked =  value == "true"
    }

    @JvmStatic
    @BindingAdapter("setLinearVerticalAdapter")
    fun <T> setLinearVerticalAdapter(recyclerView: RecyclerView, adapter: T) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
        when (adapter) {
            is SearchAdapter -> recyclerView.adapter = adapter
            is ProductsRecyclerAdapter -> recyclerView.adapter = adapter
            is TrendsRecyclerAdapter -> recyclerView.adapter = adapter
            is ProfileItemsRecyclerAdapter -> recyclerView.adapter = adapter
            else -> throw UnsupportedOperationException("Adapter not found in setLinearVerticalAdapter method of BindingAdapters")
        }
    }

    @JvmStatic
    @BindingAdapter("setLinearHorizontalAdapter")
    fun <T> setLinearHorizontalAdapter(recyclerView: RecyclerView, adapter: T) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
        when (adapter) {
            is HomeTrendsRecyclerAdapter -> recyclerView.adapter = adapter
            is HomeProductsRecyclerAdapter -> recyclerView.adapter = adapter
            else -> throw UnsupportedOperationException("Adapter not found in setLinearHorizontalAdapter method of BindingAdapters")
        }
    }


    @JvmStatic
    @BindingAdapter("setGridSpan2Adapter")
    fun <T> setGridSpan2Adapter(recyclerView: RecyclerView, adapter: T) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 1)
        when (adapter) {
            is HomeProductsRecyclerAdapter -> recyclerView.adapter = adapter
            else -> throw UnsupportedOperationException("Adapter not found in setGridSpan2Adapter method of BindingAdapters")
        }
    }




//
//    @Suppress("UNCHECKED_CAST")
//    @JvmStatic
//    @BindingAdapter("setList", "setAdapter")
//    fun <T,E> myList(recyclerView: RecyclerView, data: MutableLiveData<ArrayList<E>>?, adapter: T?) {
//
//        if (data == null || adapter == null) return
//
//        if (data.value?.isNullOrEmpty() == false) {
//
//            recyclerView.setHasFixedSize(true)
//            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
//
//            when (adapter) {
//                is SearchAdapter -> {
//                    recyclerView.adapter = adapter
//                    val value = data.value as ArrayList<SearchData>
//                    adapter.addList(value)
//                }
//            }
//
//        }
//    }




}