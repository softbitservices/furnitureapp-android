package com.softbit.furniture.interfaces

import android.view.View
import com.softbit.furniture.models.ProductsData
import com.softbit.furniture.models.SearchData
import com.softbit.furniture.models.TrendsData

interface ClickListeners {

    interface SearchItemClickListener {
        fun onSearchItemClicked(data: SearchData)
    }

    interface ProductsItemClickListener {
        fun onProductItemClicked(data: ProductsData) {}
        fun onProductItemClicked(view: View, data: ProductsData, checked: Boolean) {}
    }

    interface TrendsItemClickListener {
        fun onTrendItemClicked(data: TrendsData) {}
        fun onTrendItemClicked(view: View, data: TrendsData, checked: Boolean) {}
    }

    interface DialogButtonClickListener {
        fun onButtonClicked(performAction: Boolean)
    }

}