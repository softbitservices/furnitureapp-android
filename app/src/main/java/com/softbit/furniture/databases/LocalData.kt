package com.softbit.furniture.databases

import com.softbit.furniture.R
import com.softbit.furniture.enums.ProfileItems.*
import com.softbit.furniture.models.ProfileItemsModel

object LocalData {

    fun getProfileData(): ArrayList<ProfileItemsModel> {
        return mutableListOf(
                ProfileItemsModel(R.drawable.ic_favorite_filled, R.string.create_a_post, CREATE_POST),
                ProfileItemsModel(R.drawable.ic_home_filled, R.string.my_products, MY_PRODUCTS),
                ProfileItemsModel(R.drawable.ic_person_filled, R.string.accounts, ACCOUNTS),
                ProfileItemsModel(R.drawable.ic_wishlist_filled, R.string.about_us, ABOUT_US)
        ) as ArrayList<ProfileItemsModel>
    }

}