package com.softbit.furniture.databases

import android.content.Context
import com.softbit.furniture.enums.UserType
import com.softbit.furniture.models.LoginModel
import com.softbit.furniture.utils.getUserEditor
import com.softbit.furniture.utils.getUserPreferences

object Session {

    const val USER_TYPE_CUSTOMER = "0"
    const val USER_TYPE_BUSINESS = "1"

    const val USER_PREFERENCES = "user_pref"

    private const val LOGGED_IN = "logged_in"
    private const val USER_ID = "user_pkid"
    private const val USERNAME = "username"
    private const val USER_TYPE = "user_type"
    private const val EMAIL = "email"
    private const val CITY = "city"
    private const val COUNTRY = "country"
    private const val GENDER = "gender"
    private const val IMAGES = "images"
    private const val SMALL_IMAGES = "small_images"

    fun Context.saveUserSession(model: LoginModel, email: String) {
        getUserEditor().apply {
            putBoolean(LOGGED_IN, true)
            putString(EMAIL, email)
            putString(USER_ID, model.user_pkid)
            putString(USERNAME, model.username)
            putString(USER_TYPE, model.user_type)
            putString(CITY, model.city)
            putString(COUNTRY, model.country)
            putString(GENDER, model.gender)
            putString(IMAGES, model.images)
            putString(SMALL_IMAGES, model.small_images)
            apply()
        }
    }

    fun Context.clearUserSession() = getUserEditor().apply {
        clear()
        apply()
    }

    fun Context.isUserLoggedIn() : Boolean = getUserPreferences().getBoolean(LOGGED_IN, false)

    fun Context.getUserId(): String = getUserPreferences().getString(USER_ID, "")!!

    fun Context.getUsername(): String = getUserPreferences().getString(USERNAME, "")!!

    fun Context.getUserType(): String = getUserPreferences().getString(USER_TYPE, UserType.USER.toString())!!

    fun Context.getUserCity(): String = getUserPreferences().getString(CITY, "")!!

    fun Context.getUserCountry(): String = getUserPreferences().getString(COUNTRY, "")!!

    fun Context.getUserGender(): String = getUserPreferences().getString(GENDER, "")!!

    fun Context.getUserImages(): String = getUserPreferences().getString(IMAGES, "")!!

    fun Context.getUserSmallImages() : String = getUserPreferences().getString(SMALL_IMAGES, "")!!


}